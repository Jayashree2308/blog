class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presence_of :post_id #ensures field is not blank
	validates_presence_of :body
end
