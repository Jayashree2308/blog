class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validates_presence_of :title  #ensures these fields are not blank
	validates_presence_of :body
end
